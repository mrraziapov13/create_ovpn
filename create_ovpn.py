import os 
from sys import platform


def init():
    if platform == "linux":
        separator = "/"
    else:
        separator = "\\"
    curr_path = os.getcwd()
    files = get_full_files(curr_path)
    dirs = []
    for i in files:
        if os.path.isdir(i):
            dirs.append(i)
    all = {}
    for i in dirs:
        ca = "ca.crt"
        ta = "ta.key"
        crt_cli = i + ".crt"
        key_cli = i + ".key"
        conf = i + ".conf"
        all[i] = (ca, ta, crt_cli, key_cli, conf)
    return (all, separator, dirs, curr_path)

def get_full_files(dir):
    files = os.listdir(dir)
    return files    

def search_problem(cli_files, separator, dirs, curr_path):
    for i in cli_files.keys():
        list_files = cli_files.get(i)
        for j in list_files:
            curr = curr_path + separator + i + separator + j
            if not os.path.exists(curr):
                print("file " + curr + " is not exists")
                raise Exception
            curr_files = os.listdir(os.getcwd() + separator + i)
            if j not in curr_files:
                print("file " + j + " is not found")
                raise Exception

def create_conf_file(separator, client):
    
    if os.path.exists(os.getcwd() + separator + client + separator + "conf.txt"):
        with open(os.getcwd() + separator + client + separator + "conf.txt", "r") as file:
            conf = file.read()
        return conf
    else:
        print("folder " + client)
        print("server: ")
        server = input()
        print("port: ")
        port = input()
        with open(os.getcwd() + separator + client + separator + "conf.txt", "w") as file:
            file.write(
                "client\n"+\
                "dev tun\n"+\
                "proto udp\n"+\
                "remote " + server + " " + port + "\n"+\
                "resolv-retry infinite\n"+\
                "nobind\n"+\
                "user nobody\n"+\
                "group nogroup\n"+\
                "persist-key\n"+\
                "persist-tun\n"+\
                "remote-cert-tls server\n"+\
                "key-direction 1\n"+\
                "cipher AES-256-CBC\n"+\
                "comp-lzo\n"+\
                "verb 3\n"
            )
        with open(os.getcwd() + separator + client + separator + "conf.txt", "r") as file:
            conf = file.read()
        return conf

def get_key(lst, separator, client):
    keys = []
    k = ""

    ca_begin = "-----BEGIN CERTIFICATE-----"
    ca_end = "-----END CERTIFICATE-----"
    ta_begin = "-----BEGIN OpenVPN Static key V1-----"
    ta_end = "-----END OpenVPN Static key V1-----"
    crt_begin = "-----BEGIN CERTIFICATE-----"
    crt_end = "-----END CERTIFICATE-----"
    key_begin = "-----BEGIN ENCRYPTED PRIVATE KEY-----"
    key_end = "-----END ENCRYPTED PRIVATE KEY-----"

    ca = ""
    ta = ""
    crt = ""
    key = ""
    

    with open(os.getcwd() + separator + client + separator + lst[0], "r") as file:
        ca_text = file.read()
    ca = "\n<ca>\n" + ca_begin + "".join("".join(ca_text.split(ca_begin)[1]).split(ca_end)[0]) + ca_end + "\n</ca>\n"
    keys.append(ca)

    with open(os.getcwd() + separator + client + separator + lst[1], "r") as file:
        ta_text = file.read()
    ta = "\n<tls-auth>\n" + ta_begin + "".join("".join(ta_text.split(ta_begin)[1]).split(ta_end)[0]) + ta_end + "\n</tls-auth>\n"
    keys.append(ta)

    with open(os.getcwd() + separator + client + separator + lst[2], "r") as file:
        u_crt = file.read()
    crt = "\n<cert>\n" + crt_begin + "".join("".join(u_crt.split(crt_begin)[1]).split(crt_end)[0]) + crt_end + "\n</cert>\n"
    keys.append(crt)

    with open(os.getcwd() + separator + client + separator + lst[3], "r") as file:
        key_text = file.read()
    key = "\n<key>\n" + key_begin + "".join("".join(key_text.split(key_begin)[1]).split(key_end)[0]) + key_end + "\n</key>\n"
    keys.append(key)

    for i in keys:
        k += i + "\n"
    return k



def main():
    (cli_files, separator, dirs, curr_path) = init()
    search_problem(cli_files, separator, dirs, curr_path)
    for i in cli_files.keys():
        conf = create_conf_file(separator, i)
        keys = get_key(cli_files.get(i), separator, i)
        with open(os.getcwd() + separator + i + separator + i +".ovpn", "w") as file:
            file.write(conf + "\n\n" + keys)
if __name__ == '__main__':
    main()
